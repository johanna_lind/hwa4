// Kasutatud kirjandus:
// http://enos.itcollege.ee/~jpoial/algoritmid/Lfraction/Lfraction.html#clone()

import java.text.DecimalFormat;
import java.util.*;
import java.lang.*;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

    /**
     * Main method. Different tests.
     */
    public static void main(String[] param) throws CloneNotSupportedException {
        Lfraction test = new Lfraction(3, 2);
        Lfraction test1 = new Lfraction(2, 4);
        System.out.println(test.integerPart());
        //System.out.println(test.getDenominator());
        //System.out.println(test.toString());;

        //System.out.println(f1.compareTo(f2));
        //System.out.println(test.fractionPart());
        //System.out.println(test.divideBy(test1));

        //System.out.println(test.clone().equals(test));
        //System.out.println(valueOf("2/5").getNumerator());
        //System.out.println(toLfraction(3.14, 7));

    }

    private long numerator;
    private long denominator;
    private long first;

    /**
     * Constructor.
     *
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        if (b == 0) {
            throw new RuntimeException("Denominator ei tohi olla null");
        }
        this.numerator = a;
        this.denominator = b;
    }

    public Lfraction(long a, long b, long c) {
        if (b == 0) {
            throw new RuntimeException("Denominator ei tohi olla null");
        }
        this.numerator = a;
        this.denominator = b;
        this.first = c;
    }

    /**
     * Public method to access the numerator field.
     *
     * @return numerator
     */
    public long getNumerator() {  // getter
        return numerator; //
    }

    /**
     * Public method to access the denominator field.
     *
     * @return denominator
     */
    public long getDenominator() {  // getter
        return denominator;
    }

    public long getFirst() {  // getter
        return first;
    }

    public void setFirst(long first) {  // setter
        this.first = first;
    }

    /**
     * Conversion to string.
     *
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        return (String.valueOf(this.getNumerator()) + "/" + String.valueOf(this.getDenominator()));
    }

    /**
     * Equality test.
     *
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals(Object m) {
        //if (this.getDenominator() == ((Lfraction) m).getDenominator()) {
        //return this.getNumerator() == ((Lfraction) m).getNumerator();
        //}
        long first = ((Lfraction) m).getNumerator();
        long second = ((Lfraction) m).getDenominator();
        //return ((double)this.getNumerator() / (double)this.getDenominator()) == ((double)first / (double)second);

        Lfraction help = new Lfraction(first, second);
        return (this.compareTo(help) == 0);
    }

    /**
     * Hashcode has to be equal for equal fractions.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {

        double hashcode = (double)this.getNumerator() / (double)this.getDenominator() * 100;
        int result = (int) hashcode;
        return result;
    }

    /**
     * Sum of fractions.
     *
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {
        // korruta denominatorid läbi ja leia ühine nimetaja
        Lfraction result = new Lfraction(this.getNumerator() * m.getDenominator() + m.getNumerator() * this.getDenominator(), this.getDenominator() * m.getDenominator());

        return result;
    }

    /**
     * Multiplication of fractions.
     *
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
        Lfraction result = new Lfraction(this.getNumerator() * m.getNumerator(), this.getDenominator() * m.getDenominator());

        for (int i = (int)result.getNumerator(); i > 0; i--) {
            if (result.getNumerator() % i == 0 && result.getDenominator() % i == 0) {
                Lfraction result1 = new Lfraction(this.getNumerator() * m.getNumerator() / i, this.getDenominator() * m.getDenominator() / i);
                return result1;
            }
        }
        return result;
    }

    /**
     * Inverse of the fraction. n/d becomes d/n.
     *
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        if (this.getNumerator() == 0) {
            throw new RuntimeException("Denominator ei saa olla null");
        }

        if (this.getNumerator() < 0) {
            Lfraction result = new Lfraction(-this.getDenominator(), -this.getNumerator());  // miinus vahetab asukohta
            return (result);
        }
        Lfraction result = new Lfraction(this.getDenominator(), this.getNumerator());
        return (result);
    }

    /**
     * Opposite of the fraction. n/d becomes -n/d.
     *
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        Lfraction result = new Lfraction(-this.getNumerator(), this.getDenominator());

        return (result);
    }

    /**
     * Difference of fractions.
     *
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
        // korruta denominatorid läbi ja leia ühine nimetaja
        //Lfraction result = new Lfraction(this.getNumerator() * m.getDenominator() - m.getNumerator() * this.getDenominator(), this.getDenominator() * m.getDenominator());

        Lfraction help = new Lfraction(m.getNumerator(), m.getDenominator());
        Lfraction result = this.plus(help.opposite());

        return result;
    }

    /**
     * Quotient of fractions.
     *
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {

        //if (m.getNumerator() == 0 || m.getDenominator() == 0) {
        //throw new RuntimeException("Nulliga jagamine");
        //}
        //Lfraction result = new Lfraction(this.getNumerator() * m.getDenominator(), this.getDenominator() * m.getNumerator());

        Lfraction help = new Lfraction(m.getNumerator(), m.getDenominator());
        Lfraction result = this.times(help.inverse());

        return result;
    }

    /**
     * Comparision of fractions.
     *
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
        int result = 0;

        if (this.getDenominator() == m.getDenominator()) {  // kui denominator on sama, võrdle ainult ülemisi
            if (this.getNumerator() < m.getNumerator()) {
                return -1;
            };
            if (this.getNumerator() == m.getNumerator()) {
                return 0;
            };
            if (this.getNumerator() > m.getNumerator()) {
                return 1;
            };
        }

        if ((double)this.getNumerator() / (double)this.getDenominator() < (double)m.getNumerator() / (double)m.getDenominator()) {
            result =  -1;
        }
        if ((double)this.getNumerator() / (double)this.getDenominator() == (double)m.getNumerator() / (double)m.getDenominator()) {
            result = 0;
        }
        if ((double)this.getNumerator() / (double)this.getDenominator() > (double)m.getNumerator() / (double)m.getDenominator()) {
            result = 1;
        }
        return result;
    }

    /** Clone of the fraction.
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {

        return new Lfraction(this.getNumerator(), this.getDenominator());
    }


    /** Integer part of the (improper) fraction.
     * @return integer part of this fraction
     */
    public long integerPart () {
        if (this.getNumerator() < 0 || this.getDenominator() < 0) {  // negatiivse väärtuse jaoks
            this.setFirst((int) (this.getNumerator() / this.getDenominator()));
            return this.getFirst();
            //return (int) (this.getNumerator() / this.getDenominator());
        }
        if (this.getNumerator() < this.getDenominator()) {
            this.setFirst(0);
            return this.getFirst();
            //return 0;
        } else if (this.getNumerator() == this.getDenominator()) {
            this.setFirst(1);
            return this.getFirst();
            //return 1;
        } else {
            this.setFirst((int) (this.getNumerator() / this.getDenominator()));
            return this.getFirst();
            //return (int) (this.getNumerator() / this.getDenominator());
        }
    }

    /** Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart () {
        if (this.getNumerator() < 0 || this.getDenominator() < 0) {  // negatiivse väärtuse jaoks
            Lfraction fraction = new Lfraction(this.getNumerator() - this.integerPart() * this.getDenominator(), this.getDenominator());
            return fraction;
        }
        if (this.getNumerator() <= this.getDenominator()) {
            Lfraction fraction1 = new Lfraction(this.getNumerator(), this.getDenominator());
            return fraction1;
        } else {
            Lfraction fraction2 = new Lfraction(this.getNumerator() - this.integerPart() * this.getDenominator(), this.getDenominator());
            return fraction2;
        }
    }

    /** Approximate value of the fraction.
     * @return numeric value of this fraction
     */
    public double toDouble () {
        double result = (double)this.getNumerator() / (double)this.getDenominator();
        return result;
    }

    /** Double value f presented as a fraction with denominator d > 0.
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction ( double f, long d){
        double first = (f * (double)d);
        String result1 = String.format("%.0f", first);  // ümardamiseks

        Lfraction result = new Lfraction((Long.parseLong(result1)), d);
        return result;
    }

    /** Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf (String s){
        String[] result1 = s.split("/");
        Lfraction result = new Lfraction(Long.parseLong(result1[0]), Long.parseLong(result1[1]));  // string to long

        return result;
    }
}

